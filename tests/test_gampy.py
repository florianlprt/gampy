#!/usr/bin/env python3
''' Gampy unit tests '''

import os
import filecmp
import unittest

from gampy import Gampy

class TestGampy(unittest.TestCase):

    def test_init(self):
        g = Gampy(source_path='.', experiment='test')
        self.assertEqual(g.simulation, {'sourcePath': '.', 'experiment': 'test'})
        self.assertEqual(len(g.parameters), 0)
        self.assertEqual(len(g.outputs), 0)

    def test_init_with_extras(self):
        g = Gampy(source_path='.', experiment='test', until='length(test)=0', final_step=42)
        self.assertEqual(g.simulation, {'sourcePath': '.', 'experiment': 'test', 'until': 'length(test)=0', 'finalStep': '42'})
        self.assertEqual(len(g.parameters), 0)
        self.assertEqual(len(g.outputs), 0)

    def test_add_parameter(self):
        g = Gampy(source_path='.', experiment='test')
        g.add_parameter(name='p_test', value=42, p_type='int')
        self.assertEqual(len(g.parameters), 1)
        self.assertEqual(g.parameters[0], {'name': 'p_test', 'value': '42', 'type': 'INT'})

    def test_add_output(self):
        g = Gampy(source_path='.', experiment='test')
        g.add_output('o_test')
        self.assertEqual(len(g.outputs), 1)
        self.assertEqual(g.outputs[0], {'name': 'o_test', 'framerate': '1'})

    def test_run(self):
        g = Gampy(source_path='../example/example.gaml', experiment='test', final_step=100)
        g.add_parameter(name='myparameter', value=500, p_type='int')
        g.add_output('mymonitor')
        g.run('out_test', gama='../gama/')
        self.assertTrue(os.path.isfile('sim.xml'))
        self.assertTrue(os.path.isdir('out_test'))
        self.assertTrue(filecmp.cmp('sim.xml', 'test_sim.xml'))

if __name__ == '__main__':
    unittest.main()
