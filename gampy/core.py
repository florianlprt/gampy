'''
    Gampy

    Run Gama simulations from python
'''

import subprocess

from . import parser as p
from . import settings as s

class Gampy():
    ''' Gampy class definition '''

    def __init__(self, source_path, experiment, **kwargs):
        self.simulation = {
            s.SOURCE_PATH: source_path,
            s.EXPERIMENT: experiment,
            **{p.to_camel_case(k): str(v) for k, v in kwargs.items()}}
        self.parameters = []
        self.outputs = []

    def add_parameter(self, name, value, p_type):
        ''' Add a simulation parameter '''
        self.parameters.append({
            s.NAME: name,
            s.VALUE: str(value),
            s.P_TYPE: p_type.upper()})

    def add_output(self, name, framerate=1):
        ''' Add a simulation output '''
        self.outputs.append({
            s.NAME: name,
            s.FRAMERATE: str(framerate)})

    def run(self, output_dir, gama='.', memory=2048, hpc=None):
        ''' Parse to XML and run a Gama simulation '''
        p.to_xml(self.simulation, self.parameters, self.outputs)
        subprocess.call(s.JAVA_CMD.format(
            xml=s.SIM_XML,
            out=output_dir,
            gama=gama,
            mem=memory,
            hpc='-hpc {}'.format(hpc) if hpc else ''), shell=True)
