'''
    Gampy

    Settings
'''

# Constants (common XML tags)
#########################################
OUTPUT = 'Output'
OUTPUTS = 'Outputs'
PARAMETER = 'Parameter'
PARAMETERS = 'Parameters'
SIMULATION = 'Simulation'

# Constants (common XML attributes)
#########################################
EXPERIMENT = 'experiment'
FINAL_STEP = 'finalStep'
FRAMERATE = 'framerate'
ID = 'id'
NAME = 'name'
P_TYPE = 'type'
SOURCE_PATH = 'sourcePath'
VALUE = 'value'

# Constants (others)
#########################################
SIM_XML = 'sim.xml'

# Run
#########################################
JAVA_CMD = 'java -cp {gama}/plugins/org.eclipse.equinox.launcher*.jar \
            -Xms512m -Xmx{mem}m \
            -Djava.awt.headless=true org.eclipse.core.launcher.Main \
            -application msi.gama.headless.id4 {hpc} {xml} {out}'
