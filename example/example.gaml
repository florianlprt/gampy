/**
* Name: gampyTest
*/

model gampyTest

global {
	int foo <- 200;
	init { create bar number: foo; }
	reflex end when: foo <= 0 { do halt; } 
}

species bar {
	int x <- rnd(100000);
	reflex half { x <- int(x / 2); }
	reflex end when: x <= 0 { foo <- foo - 1; do die; }
}

experiment test type: gui {
	parameter "myparameter" var: foo;
	output {
		monitor "mymonitor" value: foo;
	}
}
