from gampy import Gampy

g = Gampy(source_path='example.gaml', experiment='test', final_step=100)
g.add_parameter(name='myparameter', value=500, p_type='int')
g.add_output('mymonitor')
g.run('out_test', gama='~/Apps/Gama-1.8RC1')
